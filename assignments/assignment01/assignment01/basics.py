# -*- coding: utf-8 -*-
print("assignment01.basics")

# Hallo Test

    
def logic_gate_and(a: any, b: any) -> bool:
    """Task01
    implement a the logic gate AND. This function should work for all python types 
    meaning that not only boolean types will be passed as variables but also numbers lists, strings, etc.

    Input 	Output
    A 	B 	Q
    0 	0 	0
    0 	1 	0
    1 	0 	0
    1 	1 	1 

    See table https://en.wikipedia.org/wiki/Logic_gate

    Args:
        a (any): unknown python type which can be expected to be conversable into bool
        b (any): unknown python type which can be expected to be conversable into bool
    
    Returns:
        bool: result of logic operation
    """
    if a and b :
        return True    
    return False
# das habe ich super gemacht

# for x in [0,1]

print (logic_gate_and(0,0))
print (logic_gate_and(1,0))
print (logic_gate_and(0,1))
print (logic_gate_and(1,1))


def logic_gate_or(a: any, b: any) -> bool:
    """Task02
    implement a the logic gate OR. This function should work for all python types 
    meaning that not only boolean types will be passed as variables but also numbers lists, strings, etc.

    Input 	Output
    A 	B 	Q
    0 	0 	0
    0 	1 	1
    1 	0 	1
    1 	1 	1 

    See table https://en.wikipedia.org/wiki/Logic_gate

    Args:
        a (any): unknown python type which can be expected to be conversable into bool
        b (any): unknown python type which can be expected to be conversable into bool
    
    Returns:
        bool: result of logic operation
    """
    if a or b :
        return True    
    return False
# das habe ich super gemacht

print (logic_gate_or(0,0))
print (logic_gate_or(1,0))
print (logic_gate_or(0,1))
print (logic_gate_or(1,1))


def logic_gate_xnor(a: any, b: any) -> bool:
    """Task03
    implement a the logic gate XNOR. This function should work for all python types 
    meaning that not only boolean types will be passed as variables but also numbers lists, strings, etc.

    Input 	Output
    A 	B 	Q
    0 	0 	1
    0 	1 	0
    1 	0 	0
    1 	1 	1 

    See table https://en.wikipedia.org/wiki/Logic_gate

    Args:
        a (any): unknown python type which can be expected to be conversable into bool
        b (any): unknown python type which can be expected to be conversable into bool
    
    Returns:
        bool: result of logic operation
        """
    if not a and not b or a and b: 
        return True
    return False
# das habe ich super gemacht


def natural_number_filter(input_list: list) -> tuple[list]:
    """Task04
    implement a function that sorts the input list into positiv and negative integers! Zero is considered both, positiv and negative and must be included in both resulting sets
    Check for any NaN (Not a Number) values and filter them out.

    Args:
        input_list (list): List of unsorted values 
    
    Returns:  
        tuple[list]: 3 lists, the first containing the positiv, the second the negative numbers and the thrid all filtered values.
    """
    positives = []
    negatives = []
    numlist=[]

    for nums in input_list:
        if type(nums)==int:
            numlist.append(nums)

    
    for num in numlist:
        if num >= 0:
            positives.append(num)
        if num <= 0:
            negatives.append(num)
    return (positives,negatives,numlist)


def remove_duplicates(input_list: list) -> list:
    """Task05
    implement a function which removes duplicate values from a list. Duplicates are any values which are True by "==" comparison 
    Hint: There is a super easy solution but if you are not yet familiar with python use a loop in your solution

    Args:
        input_list (list): list to be cleaned 

    Returns:
        list: list without duplicates
    """
    result = [] 

    for x in input_list: 
        if x not in result: 
            result.append(x) 
    return result
# das habe ich super gemacht


def wanted_poster(name: str, age: int, bounty: float) -> str:
    """Task06
    Write a wanted poster text including the personal information given as parameters

    Args:
        name (str): firstname and lastname of the searched person
        age (int): estimated age of the person
        bounty (float): bounty in $ (might be a small amount e.g. for candy theft, thus cents are given too)
    
    Returns: 
        str: wanted poster text ( be creative:) )
    """
    return f"{name} im Alter von {age} gesucht. Belohnung : {bounty} € "
# das habe ich super gemacht


def transposition_matrix(matrix: list[list]) -> list[list]:
    """Task07 (optional)
    to transposition a matrix its rows and columns are changed. The first column becomes the first row and so forth
    1 1 1      1 0 0
    0 1 0  ->  1 1 1
    0 1 0      1 0 0

    Args:
        matrix (list[list]): input example [[1,1,1], [0,1,0], [0,1,0]]

    Returns:
        list[list]: transpositioned matrix. output example [[1,0,0], [1,1,1], [1,0,0]]
    """

    ...
