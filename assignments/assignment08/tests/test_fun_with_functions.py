# -*- coding: utf-8 -*-

from assignment08.fun_with_functions import fibonacci_recursive, memorize, write_a_generator


def test_fibonacci_recursive():
    assert fibonacci_recursive(0) == -1
    assert fibonacci_recursive(1) == 0
    assert fibonacci_recursive(2) == 1
    assert fibonacci_recursive(3) == 1
    assert fibonacci_recursive(4) == 2
    assert fibonacci_recursive(5) == 3
    assert fibonacci_recursive(6) == 5
    assert fibonacci_recursive(7) == 8


def add(*args):
    return reduce(lambda x, y: x+y, args)


def test_memorize():
    new_add = memorize(add)
    assert new_add(1, 2, 3) == 6
    print(new_add.memory)
    assert new_add.memory[(1, 2, 3)] == 6
