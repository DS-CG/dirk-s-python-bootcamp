# -*- coding: utf-8 -*-
from assignment01.basics import logic_gate_and, logic_gate_or, logic_gate_xnor, natural_number_filter, remove_duplicates, wanted_poster, transposition_matrix


def test_logic_gate_and():
    assert logic_gate_and(True, True)
    assert not logic_gate_and(True, False)
    assert not logic_gate_and(False, True)
    assert not logic_gate_and(False, False)
    assert logic_gate_and(1, -9)
    assert logic_gate_and("something", True)
    assert not logic_gate_and([], True)
    assert not logic_gate_and(None, True)


def test_logic_gate_or():
    assert logic_gate_or(True, True)
    assert logic_gate_or(True, False)
    assert logic_gate_or(False, True)
    assert not logic_gate_or(False, False)


def test_logic_gate_xnor():
    assert logic_gate_xnor(True, True)
    assert logic_gate_xnor(False, False)
    assert not logic_gate_xnor(True, False)
    assert not logic_gate_xnor(False, True)
    assert logic_gate_xnor(1, -9)


def test_natural_number_filter():
    result = natural_number_filter(
        [1, 2, 42, -1, -2, -3, -4, 0, 0, "string", list(), 0.5, -0.5])
    assert type(result) is tuple and len(result) == 3
    l1, l2, l3 = result
    assert 1 in l1 and 2 in l1 and 42 in l1 and 0 in l1
    assert len(l1) == 5 and l1.count(0) == 2
    assert -1 in l2 and -2 in l2 and -3 in l2 and -4 in l2 and 0 in l2
    assert len(l2) == 6 and l1.count(0) == 2
    ### assert "string" in l3 and [] in l3 and 0.5 in l3 and -0.5 in l3


def test_remove_duplicates():
    result = remove_duplicates([45, 87, 98, 87, "a", "b", "a"])
    assert result.count(87) == 1 and result.count("a") == 1
    assert set([45, 87, 98, 87, "a", "b", "a"]) == set(result)


def test_wanted_poster():
    text = wanted_poster("Alice Wonderland", 7, 0.5)
    assert "Alice Wonderland" in text and "7" in text and "0.5" in text
    assert len("Alice Wonderland" + str(7) + str(0.5)) < len(text)


def test_transposition_matrix():
    assert transposition_matrix(transposition_matrix(
        [[0, 0, 1], [1, 1, 1], [0, 0, 1]])) == [[0, 0, 1], [1, 1, 1], [0, 0, 1]]
