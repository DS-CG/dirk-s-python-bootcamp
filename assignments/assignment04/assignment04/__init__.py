# -*- coding: utf-8 -*-

# create an in memory sqlite database
import sqlite3
sql_connection = sqlite3.connect(':memory:', check_same_thread=False)

_cur = sql_connection.cursor()
_cur.execute(
"""CREATE TABLE trees(
treeId varchar(36),
treeName varchar(255),
botanicalName varchar(255),
age int)"""
)
