
# Installation & Getting Started
- Install rancher-desktop https://docs.rancherdesktop.io/getting-started/installation/ 
- Recommaned to install WSL, but powershell will work too
- Install kubectl: [windows install](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/) and [linux install](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- Nerdctl will be installed alongside rancher-desktop
- Read
    - [Official k8s about custom resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
    - [Offical k8s docs about CRDs](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/)
    - [Arricel about exactly what we want to do](https://medium.com/@minimaldevops/crds-in-kubernetes-c38037315548)
    - [Pretty neat guide (further below) about Dockerfile and FastAPI](https://hub.docker.com/r/tiangolo/uvicorn-gunicorn-fastapi)
    - [python kubernetes annotation example](https://github.com/kubernetes-client/python/blob/master/examples/annotate_deployment.py)

## How to build a container image 
Note: yes, there is a Dockerfile but we do not build a docker image cause it is a company bound synonym for a linux container image. Do not use Docker because we need a licence for it! 

in the directory where the dockerfile is:
``nerdctl build -t plant-controller:v0.0.1 .``
Note: yes, the dot at the end is important

The -t flag gives the image a tag. The tag can be whatever you want but should be self explaining. After the colons you can set a version, if you want. 
If you need any dependencies for your controller add them to the requirements.txt


## How to apply changes to kubernetes

``kubectl apply -f FILENAME``

In the k8s-manifests directory you will find 
- CRD.yaml which must be applied once to your cluster to be able to create plant resources
- plantspaces.yaml which provide 3 namespaces with label "sunIntensity" set
- deployment.yaml which must be updated with your controller image name and version. This resource deploys a single pod of your plant-controller
- plants.yaml 2 initial plants which will be placed in the default namespace. The plant-controller should move them to an appropriate namespace


# Project Requirements

- controller deployment
- controller checks for Plants custom resources
- controller must place plants in fitting namespaces for the sun intensity of the plant (create plant in new namespace and delete in old)
    - if no fitting sun intensity namespace is found you must choose the closest one. If there are two exact same options always choose more light intensity 
- controller has to update the "lastTimeWatered" annotation of all plants. All plants are watered once a day. The amount of water is set per plant. In hour testing scenario one day is equal to one minute
- you are allowed to add more namespaces
- you are allowed to add as many plants as you want
- you are allowed to extend the requirements to your whishes